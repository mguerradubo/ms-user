import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { protobufPackage } from './users/users.pb';
import * as dotenv from 'dotenv';

dotenv.config()
async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule, 
    {
      transport: Transport.GRPC,
      options: {
        url: process.env.PORT,
        package: protobufPackage,
        protoPath: join('node_modules/protos/proto/users.proto')
      }
    }
  );
  await app.listen();
  console.log("[*] Awaiting GRPC requests");
}
bootstrap();