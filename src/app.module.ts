import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import * as dotenv from 'dotenv';

dotenv.config()
@Module({
  imports: [
    MongooseModule.forRoot(
      process.env.URI,
      {autoIndex: true}
    ),
    UsersModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
