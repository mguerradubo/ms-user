import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './user.schema';
import { Model } from 'mongoose';
import * as bcrypt from 'bcryptjs';
import { CreateUserRequest, GetUserByEmailRequest, GetUserByEmailResponse, LoginRequest } from './users.pb';


@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>
  ){}

  async createUser(user: CreateUserRequest): Promise<GetUserByEmailResponse> {
    
    const { password, ...userData } = user;
    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = new this.userModel({
      ...userData,
      password: hashedPassword,
    });

    await newUser.save();

    const Data = {
      id: newUser._id.toString(),
      ...newUser
    }
    
    return Data;
    
}

  async getUserByEmail(request: GetUserByEmailRequest): Promise<GetUserByEmailResponse> {
    const user =  await this.userModel.findOne({ email: request.email }).exec();

    const userData = {
      id: user._id.toString(),
      name: user.name,
      lastname: user.lastname,
      email: user.email,
    }
    return userData;
  }


  async login(request: LoginRequest): Promise<GetUserByEmailResponse> {
    const user = await this.userModel.findOne({ email: request.email }).exec();
    const userData = {
      id: user._id.toString(),
      ...user
    }
    const isPasswordValid = await bcrypt.compare(request.password, user.password);

    if(isPasswordValid){
      return userData;
    }
  }
  
}
