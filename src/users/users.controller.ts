import { Controller } from '@nestjs/common';
import { UsersService } from './users.service';
import { GrpcMethod } from '@nestjs/microservices';
import { CreateUserRequest, TokenResponse, GetUserByEmailRequest, GetUserByEmailResponse, LoginRequest } from './users.pb';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @GrpcMethod('UsersService', 'signUp')
  async createUser(request: CreateUserRequest): Promise<GetUserByEmailResponse> {
    return this.usersService.createUser(request);
  }

  @GrpcMethod('UsersService', 'signIn')
  async login(request: LoginRequest): Promise<GetUserByEmailResponse> {
    return this.usersService.login(request);
  }

  @GrpcMethod('UsersService', 'GetUserByEmail')
  async getUserByEmail(request: GetUserByEmailRequest): Promise<GetUserByEmailResponse> {
    return this.usersService.getUserByEmail(request);
  }
}
