import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument, ObjectId } from "mongoose";

export type UserDocument = HydratedDocument<User>

@Schema()
export class User {
    @Prop()
    id: string;

    @Prop()
    name: string;

    @Prop()
    lastname: string;

    @Prop({unique: true})
    email: string;

    @Prop()
    password: string;

}
export const UserSchema = SchemaFactory.createForClass(User);