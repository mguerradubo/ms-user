import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../users.service';
import { User } from '../user.schema';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserRequest, GetUserByEmailRequest, GetUserByEmailResponse, LoginRequest  } from '../users.pb';
import * as bcrypt from 'bcryptjs';
describe('UsersService', () => {
  let service: UsersService;
  let userModel: Model<User>

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService,
        {
          provide: getModelToken(User.name),
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    userModel =  module.get<Model<User>>(getModelToken(User.name));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('createUser', () => {
    it('should create a new user', async () => {
      const createUserInput: CreateUserRequest = {
        email: 'test@example.com',
        password: 'testPassword',
        name: 'Test User',
        lastname: 'Test lastname'
      };

      const userData = new User();
      userData.id = '1d';
      userData.name = 'Test User';
      userData.lastname = 'Test lastname';
      userData.email = 'test@example.com';
      userData.password = 'testPassword';

      expect(userData.id).toBeDefined()
      expect(userData.name).toBe(createUserInput.name);
      expect(userData.lastname).toBe(createUserInput.lastname);
      expect(userData.email).toBe(createUserInput.email);
      expect(userData.password).toBe(createUserInput.password);
      
    });
  });

  describe('getUserByEmail', () => {
    it('should find a user by email', async () => {
      const userEmail: GetUserByEmailRequest = {email: 'test@example.com'};
      const mockUser = { _id: "1d", email: 'test@example.com', name: 'Test User', lastname: 'Test lastname' };
  
      const userDocument = mockUser as any;
  
      // Crea un objeto que imite el resultado de findOne
      const findOneResult: any = {
        exec: jest.fn().mockResolvedValue(userDocument),
      };
  
      // Espía el método userModel.findOne y devuelve el objeto simulado
      const findOneSpy = jest.spyOn(userModel, 'findOne').mockReturnValue(findOneResult);
  
      const result = await service.getUserByEmail(userEmail);
  
      expect(result.email).toEqual(userDocument.email);
      expect(findOneSpy).toHaveBeenCalledWith(userEmail);
    });

    
  });

});