import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from '../users.controller';
import { UsersService } from '../users.service';
import { CreateUserRequest, GetUserByEmailRequest, GetUserByEmailResponse, LoginRequest  } from '../users.pb';
import { User } from '../user.schema';
import { getModelToken } from '@nestjs/mongoose';

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useFactory: () => ({
            createUser: jest.fn(),
            login: jest.fn(),
            getUserByEmail: jest.fn(),
            
          }),
        },
      ],
    }).compile();

    controller= module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService) as jest.Mocked<UsersService>;;

  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a new user and return GetUserByEmailResponse when createUser is called with valid input', async () => {
    // Arrange
    const request: CreateUserRequest = {
      name: 'John',
      lastname: 'Doe',
      email: 'john.doe@example.com',
      password: 'password123',
    };
    const user = {...request, id: '1'} as any;
    
    const userDocument = user as any;

    jest.spyOn(service, 'createUser').mockResolvedValue(userDocument);

    const result = await controller.createUser(request);

    expect(result.email).toEqual(user.email);
  });

          // Should successfully log in a user with valid email and password
    it('should successfully log in a user with valid email and password', async () => {
      // Arrange
      const request: LoginRequest = {
        email: 'validemail@example.com',
        password: 'validpassword',
      };
      const expectedResponse: GetUserByEmailResponse = {
        id: '1',
        name: 'John',
        lastname: 'Doe',
        email: 'validemail@example.com',
      };
      const userDocument = expectedResponse as any;

      jest.spyOn(service, 'login').mockReturnValue(userDocument);

      const result = await controller.login(request);

      // Assert
      expect(result.email).toEqual(expectedResponse.email);
      
    });
    
});
